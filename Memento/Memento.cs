﻿// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Memento
{
    public class Memento : PropertyNotifier
    {
        private List<IMementoItem> list = new List<IMementoItem>();
        private int index = -1;
        private Stack<MementoItemGroup> groups = new Stack<MementoItemGroup>();

        private bool canUndo = false;
        public bool CanUndo
        {
            get { return canUndo; }
            set { if (value != canUndo) { canUndo = value; NotifyPropertyChanged(); } }
        }

        private bool canRedo = false;
        public bool CanRedo
        {
            get { return canRedo; }
            set { if (value != canRedo) { canRedo = value; NotifyPropertyChanged(); } }
        }

        public void Begin()
        {
            MementoItemGroup parent = null;
            if (groups.Count > 0)
                parent = groups.Peek();

            MementoItemGroup child = new MementoItemGroup();
            groups.Push(child);

            if (parent != null)
            {
                parent.Add(child);                
            }
            else
            {
                index++;
                list.Add(child);
            }
        }


        public void RememberCollection<T>(ICollection<T> value)
        {
            RemoveTop();

            if (groups.Count == 0)
            {
                index++;
                list.Add(new MementoItemList<T>(value));

                CanRedo = index < list.Count - 1;
                CanUndo = index >= 0;
            }
            else
            {
                MementoItemGroup group = groups.Peek();
                group.Add(new MementoItemList<T>(value));
            }
        }

        public void RememberArray<T, V>(T obj, string name)
        {
            RemoveTop();

            if (groups.Count == 0)
            {
                index++;
                list.Add(new MementoItemArray<T, V>(obj, name, (V[])(obj.GetType().GetProperty(name).GetValue(obj, null))));

                CanRedo = index < list.Count - 1;
                CanUndo = index >= 0;
            }
            else
            {
                MementoItemGroup group = groups.Peek();
                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(name);
                V[] value = (V[])info.GetValue(obj, null);

                group.Add(new MementoItemArray<T, V>(obj, name, value));
            }
        }

        public void Remember<T, V>(T obj, string name)
        {
            RemoveTop();

            if (groups.Count == 0)
            {
                index++;
                list.Add(new MementoItem<V, T>(obj, name, (V)obj.GetType().GetProperty(name).GetValue(obj, null)));

                CanRedo = index < list.Count - 1;
                CanUndo = index >= 0;
            }
            else
            {
                MementoItemGroup group = groups.Peek();
                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(name);
                V value = (V)info.GetValue(obj, null);

                group.Add(new MementoItem<V, T>(obj, name, value));
            }
        }

        public void End()
        {
            groups.Pop();
            CanUndo = true;
        }

        public void End(EndDelegate ended)
        {
            groups.Peek().Ended += ended;
            groups.Pop();
            CanUndo = true;
        }

        public void Undo()
        {
            int i = index;
            index--;

            CanRedo = index < list.Count - 1;
            CanUndo = index >= 0;

            list[i].Swap();
        }

        public void Redo()
        {
            index++;

            CanRedo = index < list.Count - 1;
            CanUndo = index >= 0;

            list[index].Swap();
        }


        private void RemoveTop()
        {
            while (index < list.Count - 1)
                list.RemoveAt(list.Count - 1);
        }
    }
}
